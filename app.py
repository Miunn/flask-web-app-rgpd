# coding: utf-8

from dotenv import load_dotenv

import re

from flask import Flask, render_template, request, redirect, url_for
from db import *
from utils import isInt

load_dotenv()

# Création de l'application web Flask
app = Flask(__name__)


""" Définition des endpoints d'écoute """


@app.route('/', methods=['GET'])
def home():
    return render_template('index.html')


@app.route('/checkform', methods=["POST"])
def submit_form():

    fname = request.form.get("fname")
    lname = request.form.get("lname")
    birthdate = request.form.get("birthdate")
    birthplace = request.form.get("placeBirth")
    degree = request.form.get("degreeLvl")
    mail = request.form.get("mail")
    address = request.form.get("address")
    ssn = request.form.get("socSecNumber")
    phoneNumber = request.form.get("phonenumber").strip().replace(" ", "")
    submissionValid = True

    if not re.match("^((0[1-9])|((1|2)[0-9])|(3(0|1)))\/((0[1-9])|(1[0-2]))\/((19[0-9]{2})|(20(0|1)[0-9]{1}))$", birthdate):
        submissionValid = False

    if not re.match("^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$", mail):
        submissionValid = False

    if len(ssn) != 15 or (ssn[0] != "1" and ssn[0] != "2"):
        submissionValid = False

    if not ((phoneNumber[0] == "+" and (len(phoneNumber[3:]) == 9) and isInt(phoneNumber[1:])) or (len(phoneNumber) == 10 and phoneNumber[0] == "0" and isInt(phoneNumber))):
        submissionValid = False

    if submissionValid:

        insert_submit(fname, lname, birthdate, birthplace,
                      degree, address, mail, ssn, phoneNumber)

        return render_template("submittedForm.html")
    else:
        return redirect(url_for('home'), code=307)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)
