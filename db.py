# coding: utf-8

from os import getenv
import mariadb

# Installation connecteur MySQL: python -m pip install mysql-connector-python
# https://gayerie.dev/docs/python/python3/mysql.html
# https://mariadb.com/fr/resources/blog/how-to-connect-python-programs-to-mariadb/


def get_db_params() -> dict:
    return {
        "host": getenv("DB_HOST"),
        "port": int(getenv("DB_PORT")),
        "user": getenv("DB_LOGIN"),
        "password": getenv("DB_PSWD"),
        "database": getenv("DB_NAME")
    }


def insert_submit(fname: str, lname: str, bdate: str, bplace: str, degree: str, postaladdress: str, mail: str, ssn: int, phonenb: str):
    query = """insert into submits
            (firstname, lastname, birthdate, birthplace, degreelvl,
             postaladdress, mail, socSecNumber, phonenumber)
            values (%s, %s, %s, %s, %s, %s, %s, %s, %s)"""
    values = (fname, lname, bdate, bplace, degree,
              postaladdress, mail, ssn, phonenb)

    with mariadb.connect(**get_db_params()) as db:
        # db.autocommit = False
        with db.cursor() as crsr:
            crsr.execute(query, values)
            db.commit()

            print("Transaction done")
