function checkBDate(input) {
    var val = input.value;

    if (val.length == 0) {
        return;
    }

    var elmt = document.getElementById("birthdate");

    if (val.length == 2 || val.length == 5) {
        elmt.value += "/";
    }
    else if ((val.length == 3 || val.length == 6) && val[val.length - 1] != '/') {
        elmt.value = val.substring(0, val.length - 1) + "/" + val[val.length - 1]
    }
    else if (val.length == 3 || val.length == 6) {
        elmt.value = val.substring(0, val.length - 1);
    }

    if (/^((0[1-9])|((1|2)[0-9])|(3(0|1)))\/((0[1-9])|(1[0-2]))\/((19[0-9]{2})|(20(0|1)[0-9]{1}))$/.test(val)) {
        return true;
    }
    else {
        return false;
    }
}

function checkMail(input) {
    var val = input.value.trim();
    if (val.length == 0) {
        return;
    }
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(val)) {
        return true;
    }
    else {
        return false;
    }
}

function checkSSN(input) {
    var val = input.value.trim();
    if (val.length == 0) {
        return;
    }
    if (val.length != 15 || (val[0] != "1" && val[0] != "2")) {
        return false;
    }
    else {
        return true;
    }
}

function checkPhoneNumber(input) {
    var val = input.value.trim();
    val = val.replaceAll(' ', '');
    if (val.length == 0) {
        return;
    }
    if ((val[0] == "+" && (val.substring(3).length == 9) && isInt(val.substring(1))) || (val.length == 10 && val[0] == "0" && isInt(val))) {
        return true;
    }
    else {
        return false;
    }
}

document.getElementById("birthdate").addEventListener('input', (event) => {
    var label = document.getElementById("label-birthdate");
    if (checkBDate(event.target)) {
        label.textContent = "Date de naissance";
        event.target.style.border = "none";
    } else {
        label.innerHTML = `Date de naissance <span style="color: red; font-style: italic;"> - Invalid</span>`;
        event.target.style.border = "1px solid red";
    }
});

document.getElementById("mail").addEventListener("input", (event) => {
    var label = document.getElementById("label-mail");
    if (checkMail(event.target)) {
        label.textContent = "Adresse mail";
        event.target.style.border = "none";
    } else {
        label.innerHTML = `Adresse mail <span style="color: red;font-style: italic;"> - Invalid</span>`;
        event.target.style.border = "1px solid red";
    }
});

document.getElementById("socSecNumber").addEventListener("input", (event) => {
    var label = document.getElementById("label-ssn");
    if (checkSSN(event.target)) {
        label.textContent = "Sécurité sociale";
        event.target.style.border = "none";
    } else {
        label.innerHTML = `Sécurité sociale <span style="color: red;font-style: italic;"> - Invalid</span>`;
        event.target.style.border = "1px solid red";
    }
});

document.getElementById("phonenumber").addEventListener("input", (event) => {
    var label = document.getElementById("label-phonenb");
    if (checkPhoneNumber(event.target)) {
        label.textContent = "Téléphone";
        event.target.style.border = "none";
    } else {
        label.innerHTML = `Téléphone <span style="color: red;font-style: italic;"> - Invalid</span>`;
        event.target.style.border = "1px solid red";
    }
});

document.getElementById("application-form").addEventListener("submit", (event) => {
    event.preventDefault();
    if (!checkBDate(document.getElementById("birthdate")) || !checkMail(document.getElementById("mail")) || !checkSSN(document.getElementById("socSecNumber")) || !checkPhoneNumber(document.getElementById("phonenumber"))) {
    } else {
        // Création formulaire avec les données saisies à POST
        const formData = new FormData(event.target);

        // Requete au serveur du formulaire
        fetch(event.target.action, {
            method: 'POST',
            body: formData
        }).then(function (response) {
            // Response Object du serveur
            return response.text();
        }).then(function (html) {
            // Récupération et affichage de la page html renvoyée par le serveur
            document.body.innerHTML = html;
        });
    }
})

function isInt(d) {
    for (var i = 0; i < d.length; i++) {
        if (!(d[i] >= "0" && d[i] <= "9")) {
            return false;
        }
    }
    return true;
}

