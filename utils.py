def isInt(d):
    for i in range(len(d)):
        if not (d[i] >= "0" and d[i] <= "9"):
            return False
    return True
